#' Get metadata from character vector (named according to GCMS experiment)
#'
#' @param str Character vector with sample names
#'
#' @return metadata with 5 columns: 'trt', 'id', 'tp (timepoint)', 'repetition', 'inj'
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
getGCMeta <- function(str) {
  str <- strsplit(str,'_')
  sampleInfo=character(4)
  for(i in 1:length(str)) {
    sample <- str[[i]]
    if(sample[1]=='QC') {
      sampleInfo=rbind(sampleInfo,c(sample[1],'ID3','TP','1'))
    } else {
      sampleInfo=rbind(sampleInfo,sample[1:4])
    }
  }
  sampleInfo <- sampleInfo[-1,]
  sampleInfo <- data.frame(sampleInfo)
  colnames(sampleInfo) <- c('trt', 'id', 'tp', 'rep')
  sampleInfo[,2] <- as.numeric(substring(sampleInfo[,2],3))
  sampleInfo[,3] <- as.numeric(substring(sampleInfo[,3],3))
  sampleInfo[,4] <- as.numeric(sampleInfo[,4])
  sampleInfo$inj=1:nrow(sampleInfo)
  return(sampleInfo)
}

#' Get metadata from character vector (named according to LCMS experiment)
#'
#' @param str Character vector with sample names
#'
#' @return metadata with 5 columns: 'trt', 'id', 'tp (timepoint)', 'repetition', 'inj'
#' @export
#'
#' @examples
#' LCNEG <- read.csv2(file='StudentData/DataFiles/LCNEG.csv',stringsAsFactors = F)
#' lcmsMeta <- getLCMeta(LCNEG$sample)
getLCMeta <- function(str) {
  str <- strsplit(str,'_')
  sampleInfo=matrix(unlist(str),byrow=T,ncol=5)
  sampleInfo <- data.frame(sampleInfo)
  colnames(sampleInfo) <- c('trt', 'id', 'tp', 'rep', 'inj')
  sampleInfo[,2] <- as.numeric(substring(sampleInfo[,2],3))
  sampleInfo[,3] <- as.numeric(substring(sampleInfo[,3],3))
  sampleInfo[,4] <- as.numeric(sampleInfo[,4])
  sampleInfo[,5] <- as.numeric(sampleInfo[,5])
  return(sampleInfo)
}

#' Get feature metadata from character vector (named according to LCMS experiment)
#'
#' @param str Character vector with feature identifiers
#'
#' @return metadata with 3 columns: 'mode', 'mz', 'rt'
#' @export
#'
#' @examples
#' LCNEG <- read.csv2(file='StudentData/DataFiles/LCNEG.csv',stringsAsFactors = F)
#' lcnegMeta <- getLCMeta(LCNEG$sample)
#' lcnegData <- LCNEG[,-1]
#' featInfo=getFeatureInfo(colnames(lcnegData))
getFeatureInfo=function(str) {
  mode <- substring(str,1,2)
  str <- substring(str,3)
  str <- strsplit(str,'_')
  featureInfo=matrix(as.numeric(unlist(str)),byrow=T,ncol=2)
  featureInfo <- data.frame(featureInfo)
  colnames(featureInfo) <- c('mz','rt')
  featureInfo <- data.frame(mode,featureInfo)
  return(featureInfo)
}

#' Sort metadata and data by trt, id, tp and rep
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#'
#' @return list object with $meta and $data
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
#' gcmsData <- GCMS[,-1]
#'
#' gc <- reSort(meta=gcmsMeta,data=gcmsData)
#' pcaG <- prcomp(gc$data,scale. = T,center=T)
#' rdCV::biplotPCA(pcaG,pch=15+gc$meta$id,colSc=gc$meta$trt,labs = gc$meta$tp)
reSort <- function(meta,data) {
  order <- with(meta,order(trt,id,tp,rep))
  meta <- meta[order,]
  data <- data[order,]
  return(list(meta=meta,data=data))
}

#' Filter out the QC samples
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#' @param pattern QC name pattern (defaults to 'QC')
#'
#' @return list object with $meta and $data
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
#' gcmsData <- GCMS[,-1]
#'
#' gc <- reSort(meta=gcmsMeta,data=gcmsData)

#' gc <- filterQC(meta=gc$meta,data=gc$data)
#' pcaG <- prcomp(gc$data,scale. = T,center=T)
#' rdCV::biplotPCA(pcaG,pch=15+gc$meta$id,colSc=gc$meta$trt,labs = gc$meta$tp)
filterQC <- function(meta,data,pattern='QC') {
  data <- data[meta$trt!=pattern,]
  meta <- meta[meta$trt!=pattern,]
  return(list(meta=meta,data=data))
}

#' Average data over repetitions
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#'
#' @return list object with $meta and $data
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
#' gcmsData <- GCMS[,-1]
#'
#' gc <- reSort(meta=gcmsMeta,data=gcmsData)
#' gc <- filterQC(meta=gc$meta,data=gc$data)
#'
#' gc <- aveSample(meta=gc$meta,data=gc$data)
#' pcaG <- prcomp(gc$data,scale. = T,center=T)
#' rdCV::biplotPCA(pcaG,pch=15+gc$meta$id,colSc=gc$meta$trt,labs = gc$meta$tp)
aveSample <- function(meta,data) {
  sample <- interaction(meta$trt,meta$id,meta$tp,sep='_')
  unik <- unique(sample)
  nUnik <- length(unik)
  mUnik <- meta[!duplicated(sample),-5]
  dUnik <- data[!duplicated(sample),]
  for (i in 1:nUnik) dUnik[i,] <- colMeans(data[meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] & meta$tp==mUnik$tp[i],])
  return(list(meta=mUnik,data=dUnik))
}

#' Subtract baseline measurement per individual
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#'
#' @return list object with $meta and $data
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
#' gcmsData <- GCMS[,-1]
#'
#' gc <- reSort(meta=gcmsMeta,data=gcmsData)
#' gc <- filterQC(meta=gc$meta,data=gc$data)
#' gc <- aveSample(meta=gc$meta,data=gc$data)
#'
#' gc <- blSub(meta=gc$meta,data=gc$data)
#' pcaG <- prcomp(gc$data,scale. = T,center=T)
#' rdCV::biplotPCA(pcaG,pch=15+gc$meta$id,colSc=gc$meta$trt,labs = gc$meta$tp)
blSub <- function(meta,data) {
  id <- interaction(meta$trt,meta$id,sep='_')
  unik <- unique(id)
  nUnik <- length(unik)
  mUnik <- meta[!duplicated(id),-5]
  for (i in 1:nUnik) {
    nRep <- sum(meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] )
    whBl=which(meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] & meta$tp==0)
    data[meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] ,] <- data[meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] ,] - data[rep(whBl,nRep),]
  }
  return(list(meta=meta,data=data))
}

#' Calculate Area-Under-the-Curve (AUC) per feaature
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#'
#' @return list object with $meta and $data
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
#' gcmsData <- GCMS[,-1]
#'
#' gc <- reSort(meta=gcmsMeta,data=gcmsData)
#' gc <- filterQC(meta=gc$meta,data=gc$data)
#' gc <- aveSample(meta=gc$meta,data=gc$data)
#' gc <- blSub(meta=gc$meta,data=gc$data)
#'
#' gc <- makeAUC(meta=gc$meta,data=gc$data)
#' pcaG <- prcomp(gc$data,scale. = T,center=T)
#' rdCV::biplotPCA(pcaG,pch=15+gc$meta$id,colSc=gc$meta$trt,labs = gc$meta$id)
makeAUC <- function(meta,data) {
  id <- interaction(meta$trt,meta$id,sep='_')
  unik <- unique(id)
  nUnik <- length(unik)
  mUnik <- meta[!duplicated(id),-5]
  dUnik <- data[!duplicated(id),]
  for (i in 1:nUnik) {
    dataSub <- data[meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] ,]
    times <- meta$tp[meta$trt==mUnik$trt[i] & meta$id==mUnik$id[i] ]
    dUnik[i,] <- apply(dataSub,2,function(x) pracma::trapz(times,x))
  }
  return(list(meta=mUnik,data=dUnik))
}

#' Average data over treatments
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#'
#' @return list object with $meta and $data
#' @export
#'
#' @examples
#' GCMS <- read.csv2(file='StudentData/DataFiles/GCMS.csv',stringsAsFactors = F)
#' gcmsMeta <- getGCMeta(GCMS$Sample)
#' gcmsData <- GCMS[,-1]
#'
#' gc <- reSort(meta=gcmsMeta,data=gcmsData)
#' gc <- filterQC(meta=gc$meta,data=gc$data)
#' gc <- aveSample(meta=gc$meta,data=gc$data)
#' gc <- blSub(meta=gc$meta,data=gc$data)
#' gc <- aveTrt(meta=gc$meta,data=gc$data)
#' pcaG <- prcomp(gc$data,scale. = T,center=T)
#' rdCV::biplotPCA(pcaG,pch=15+gc$meta$id,colSc=gc$meta$trt,labs = gc$meta$tp)
aveTrt <- function(meta,data) {
  sample <- interaction(meta$trt,meta$tp,sep='_')
  unik <- unique(sample)
  nUnik <- length(unik)
  mUnik <- meta[!duplicated(sample),-5]
  dUnik <- data[!duplicated(sample),]
  for (i in 1:nUnik) dUnik[i,] <- colMeans(data[meta$trt==mUnik$trt[i] & meta$tp==mUnik$tp[i],])
  return(list(meta=mUnik,data=dUnik))
}

#' Rank variables according to largest fold change
#'
#' @param meta metadata matrix with 'trt', 'id', 'tp', 'rep' in cols 1:4
#' @param data data matrix, row-wise matched to meta
#' @param negData Default substitution value for negative values in data matrix (defaults to 1e-2)
#'
#' @return list object with $maxFC (max absolute log2(fold change) per variable) $absLogFC (matrix with all abs log fold changes) $FC (matrix with actual fold changes)
#' @export
rankFC <- function(meta,data,negData=1e-2) {
  Trt=unique(meta$trt)
  nTrt <- length(Trt)
  nCompare=sum(1:(nTrt-1))
  FCmat=matrix(nrow=ncol(data),ncol=2*nCompare)
  names=character(2*nCompare)
  data[data<=0] <- 1e-2
  n <- 0
  for (i in 1:(nTrt-1)) {
    for (j in (i+1):nTrt) {
      n <- n+1
      names[n]=paste(Trt[i],Trt[j],'30',sep='_')
      names[n+nCompare]=paste(Trt[i],Trt[j],'90',sep='_')
      for(k in 1:ncol(data)) {
        FCmat[k,n] <- data[meta$trt==Trt[i] & meta$tp==30,k]/data[meta$trt==Trt[j] & meta$tp==30,k]
        FCmat[k,n+nCompare] <- data[meta$trt==Trt[i] & meta$tp==90,k]/data[meta$trt==Trt[j] & meta$tp==90,k]
      }
    }
  }
  colnames(FCmat)=names
  rownames(FCmat)=colnames(data)
  FCAbsLog <- abs(log2(FCmat))
  return(list(maxFC=apply(FCAbsLog,1,max), absLogFC=FCAbsLog, FC=FCmat))
}

