################################################
# Make multiclass-problem

library(MPCourse)

# Make 3 groups
groups <- makeGroups(nGroup = 3, nSampPerGroup = 70)
head(groups)
tail(groups)
View(groups)

# Make random data
nVar <- 50
randomData <- makeRandomMatrix(nSamp = 210,
                               nVar = nVar,
                               sampNames = groups$sample,
                               maxInt = 100) # Make a random matrix with 50 samples and 10 variables ranging from 0 to 100 in a uniform distribution

# PLS
library(pls)
plsda <- plsDA(X = randomData,
               Y = groups$group,
               nComp = 3)
plsda$table
plotPLSDA(plsda)

# rdCV-PLS
library(rdCV)
library(doParallel)
# Set up for parallel processing
nCore <- detectCores()-1
cl <- makeCluster(nCore)
registerDoParallel(cl)
# Make the actual model
rdCVModel <- rdCV(X = randomData,
                  Y = groups$group,
                  nRep = 30,
                  method = 'PLS',
                  fitness = 'MISS')
stopCluster(cl)
# Look at the classifications
table(actual = groups$group,
      predicted = rdCVModel$yClass)
