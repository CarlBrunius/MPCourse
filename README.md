# BBT020 Metabolomics and Proteomics
Welcome to the repository for the Data Analysis section of the course: BBT020 Metabolomics and Proteomics  

Associate Professor Carl Brunius <carl.brunius@chalmers.se>  
Department of Life Sciences
Chalmers University of Technology  

# General info
This repository contains course documents and an R package for data analysis  
- Course documents (ppt, word, data and R scripts) can be found under the `Course Documents` folder in the repository
- The R library is installed using instructions found under `Course Documents/Scripts/1_setup_and_PCA.R` OR by following the instructions below

# Installation of "everything"
After following these instructions you should have your computer completely set up for doing the computer lab!  
You will install R, RStudio and some packages, including `remotes`, `MPCourse`, `rdCV`, `pls`, `BiocManager` and `mixOmics`.  

- First download and install R from https://www.r-project.org/
- Then download and install RStudio from https://rstudio.com/

To install the relevant packages, you need to work in R. So go ahead and start RStudio. You will need to have the `remotes` R package installed. Just run the following from an R script or type it directly at the R console (normally the lower left window in RStudio):
```
install.packages('remotes')
```
When `remotes` is installed, you can simply install the `MPCourse` package like this:
```
library(remotes)
install_gitlab('CarlBrunius/MPCourse')
```
You will also need som other R packages. Just copy the code chunk below and run it from a script or directly in your R console. Please note that R may ask you to update packages to later versions. This is normally a good idea. Update all packages when prompted. However, R will sometimes tell you that there are binary versions available but that source versions are later. I normally recommend to avoid installing from source to avoid problems that sometimes occur when needing to compile from source. Just say no.
```
install.packages("BiocManager")
BiocManager::install("mixOmics")
install.packages('pls')
install_gitlab('CarlBrunius/rdCV')
```
Now you should be all set for the computer lab!

# Time to start working
I suggest to start the actual work by reading __"A Super Brief R Tutorial"__ in the `Course Documents` folder.  
In preparation for Computer lab 1, you should go through the __"Preparatory Home Assignment"__ in the `Course Documents` folder. This document provides some repetitions and code to prepare you for the actual lab. 
The main study document is called __"Data Analysis Computer Lab 1"__, also in the `Course Documents` folder. This document should contain everything you need to get started!

Happy coding!

Carl


# Version history
version | date | comment
:------ | :--- | :------
0.1.2 | 2023-03-14 | Update for BBT020 2023
0.1.1 | 2020-03-26 | Update for BBT020 2020
0.1.0 | 201x-xx-xx | Legacy version

